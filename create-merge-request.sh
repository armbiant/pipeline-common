#!/bin/bash

set -e

ROOT_DIR="$(dirname "$(readlink -e "$0")")"
# shellcheck disable=SC1091
source "${ROOT_DIR}/lib.sh"

git config --global user.name "LKFT Bot"
git config --global user.email lkft-bot@linaro.org

clone_or_update "https://${GIT_USER}:${GIT_PASS}@gitlab.com/Linaro/lkft/pipelines/common.git" origin common
clone_or_update "https://${GIT_USER}:${GIT_PASS}@gitlab.com/Linaro/lkft/pipelines/lkft-common.git" origin lkft-common

tuxsuite_project="${TUXSUITE_PROJECT:-lkft}"

if [ $# -ge 1 ]; then
  ROOTFS_TUXPLAN_ID="$1"
fi

if [ -z "${ROOTFS_TUXPLAN_ID}" ]; then
  echo "Need a Tuxplan ID in the ROOTFS_TUXPLAN_ID env variable or as argument."
  exit 1
fi

TUXSUITE_PROJECT="${tuxsuite_project}" tuxsuite plan get --json "${ROOTFS_TUXPLAN_ID}" > builds.json

if [ ! -s builds.json ]; then
  echo "Could not fetch builds with the given Tuxplan ID."
  exit 1
fi

tuxplan_id="$(jq -r .oebuilds.results[].plan builds.json | sort -u | head -n1)"
tuxplan_datetime="$(jq -r .oebuilds.results[].provisioning_time builds.json | sort | head -n1)"
branch="autobranch-$(date -u +"%Y%m%d-%H%M%S" -d "${tuxplan_datetime}")"
merge_date="$(date -u +"%Y-%m-%d" -d "${tuxplan_datetime}")"

# Changes in lkft-common
echo "# Making changes in lkft-common..."
for MACHINE in am57xx-evm fvp-base juno intel-core2-32 intel-corei7-64; do
  echo "## Machine: $MACHINE"
  download_url="$(jq -r ".oebuilds.results[] | select(.machine==\"${MACHINE}\") | .download_url" builds.json)"
  if [ -z "${download_url}" ]; then
    echo "WARNING: There is no root file system for machie ${MACHINE}."
    continue
  fi
  get_bundle_from_tuxpub "${download_url}/images/${MACHINE}" "${MACHINE}" > bundle.json
  url_image="$(get_from_bundle rootfs.tux.ext4gz)"

  case ${MACHINE} in
    am57xx-evm) arch="arm" ;;
    fvp-base) arch="fvp" ;;
    juno) arch="arm64" ;;
    intel-core2-32) arch="i386" ;;
    intel-corei7-64) arch="x86_64" ;;
  esac

  for plan_file in lkft-common/tuxconfig/linux-[456].*.y-plan.yml lkft-common/tuxconfig/master-plan.yml; do
    if grep -q "rootfs-lkft-${arch}" "${plan_file}"; then
      replace_line "${plan_file}" "rootfs-lkft-${arch}: \&rootfs-lkft-${arch} " "${url_image}"
    fi
  done
done

# Changes in common
echo "Making changes in common..."
replace_line common/common.yml "ROOTFS_TUXPLAN_ID: " "\"${tuxplan_id}\""
replace_line common/common.yml "TUXSUITE_PROJECT: " "\"${tuxsuite_project}\""

echo "Committing changes in lkft-common..."
git -C lkft-common status
git -C lkft-common checkout -B "${branch}" origin/master
git -C lkft-common add -u .
git -C lkft-common commit -s -m "tuxconfig/plans: Update Tuxplan ID (${merge_date})"
git -C lkft-common show

echo "Committing changes in common..."
git -C common status
git -C common checkout -B "${branch}" origin/master
git -C common add -u .
git -C common commit -s -m "common: Update Tuxplan ID (${merge_date})"
git -C common show

echo "Creating merge-request for common..."
git -C common push \
  -o merge_request.create \
  -o merge_request.title="[auto] Tuxplan ID: Automerge ${merge_date}" \
  -o merge_request.target=master \
  -f origin "${branch}"

echo "Creating merge-request for lkft-common..."
git -C lkft-common push \
  -o merge_request.create \
  -o merge_request.title="[auto] Tuxplan ID: Automerge ${merge_date}" \
  -o merge_request.target=master \
  -f origin "${branch}"
