#!/bin/bash

set -e

export QA_PROJECT="${QA_PROJECT//\//_}"
export QA_PROJECT_SANITY="${QA_PROJECT_SANITY//\//_}"

git_desc="$(jq -r '.builds|.[] | .git_describe' build.json | head -1)"
export git_desc

pip --quiet --no-cache-dir install squad-client

if [ -n "${QA_REPORTS_TOKEN}" ]; then
  squad-client --squad-token="${QA_REPORTS_TOKEN}" --squad-host="${QA_SERVER}" submit-tuxsuite --group="${QA_TEAM}" --project="${QA_PROJECT}" --build="${git_desc}" --backend tuxsuite.com --json build.json
else
  echo "Skip squad-client submit-tuxsuite"
fi

if [ -n "${QA_REPORTS_TOKEN}" ] && [ ! "${RUN_SANITY}" == "0" ]; then
  squad-client --squad-token="${QA_REPORTS_TOKEN}" --squad-host="${QA_SERVER}" submit-tuxsuite --group="${QA_TEAM}" --project="${QA_PROJECT_SANITY}" --build="${git_desc}" --backend tuxsuite.com --json build.json
else
  echo "Skip squad-client submit-tuxsuite for the sanity project"
fi

# Only if automated bisection is turned on.
if [ "${RUN_AUTOMATED_BUILD_BISECTION}" == "1" ]; then
  echo "Will initiate bisection..."
  ./submit-jobs-to-bisect.sh
else
  echo "Skip automated build bisection"
fi
