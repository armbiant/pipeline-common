#!/usr/bin/env bash

set -e
#set -x
set -u

# $1 parameter to extract
#    This argument is required
# $2 location of bundle.json
#    This is optional. Defaults to
#    bundle.json in the current
#    directory.
get_from_bundle() {
  bundle_json="bundle.json"
  if [ $# -gt 1 ]; then
    bundle_json="$2"
  fi
  jq -r ".$1" "${bundle_json}"
}

# Generate a file called submit-tests.sh
# to make it easier for other to reuse.
generate_submit_tests() {
  cd "${LAVA_TEST_PLANS_DIR}"
  # shellcheck disable=SC2153
  cat << EOF | tee "${WORKDIR}/submit-tests.sh"
#!/bin/bash -e
[ -d "${LAVA_TEST_PLANS_DIR}" ] && cd "${LAVA_TEST_PLANS_DIR}" || exit 1
submit_for_testing.py \\
  ${DRY_RUN} \\
  --variables "${TEST_PLANS_VAR_FILE}" \\
  --device-type "${DEVICE_TYPE}" \\
  --build-number "${BUILD_ID}" \\
  --lava-server "${LAVA_SERVER}" \\
  --qa-server "${QA_SERVER}" \\
  --qa-server-team "${QA_TEAM}" \\
  --qa-server-project "${QA_PROJECT}" \\
  ${LAVA_TESTS} \\
  ${testplan_device_path} \\
  ${QA_ENVIRONMENT}
EOF
}

# Given a DEVICE_TYPE, define ROOTFS_MACHINE
# $1: DEVICE_TYPE name. Required
devicetype_to_rootfsmachine() {
  if [ $# -lt 1 ]; then
    return 1
  fi

  local DEVICE_TYPE="$1"
  case "${DEVICE_TYPE}" in
    bcm2711-rpi-4-b)  ROOTFS_MACHINE="juno"             ;;
    dragonboard-410c) ROOTFS_MACHINE="dragonboard-410c" ;;
    dragonboard-845c) ROOTFS_MACHINE="dragonboard-845c" ;;
    hi6220-hikey)     ROOTFS_MACHINE="hikey"            ;;
    i386)             ROOTFS_MACHINE="intel-core2-32"   ;;
    juno-r2)          ROOTFS_MACHINE="juno"             ;;
    nxp-ls2088)       ROOTFS_MACHINE="ls2088ardb"       ;;
    x15)              ROOTFS_MACHINE="am57xx-evm"       ;;
    x86)              ROOTFS_MACHINE="intel-corei7-64"  ;;
    qemu_arm)         ROOTFS_MACHINE="am57xx-evm"       ;;
    qemu_arm64)       ROOTFS_MACHINE="juno"             ;;
    qemu_i386)        ROOTFS_MACHINE="intel-core2-32"   ;;
    qemu_x86_64)      ROOTFS_MACHINE="intel-corei7-64"  ;;
  esac

  export ROOTFS_MACHINE
}

check_and_write() {
  if [ -v "${1}" ] && [ -n "${1}" ]; then
    echo "${1}=${!1}" >> "${TEST_PLANS_VAR_FILE}"
  fi
}

# Assign a value to a variable if:
# * it doesn't exist, or
# * its value is empty
# $1: Variable name
# $2: Variable value
check_and_set() {
  if [ -v "${1}" ]; then
    if [ -z "${1}" ]; then
      export "${1}"="${2}"
    fi
  else
    export "${1}"="${2}"
  fi
}

# Define variables that are used by submit_for_testing.py
define_sft_vars_for_machine() {
  # DEVICE_TYPE is defined by machine_to_devicetype()
  TEST_PLANS_VAR_FILE="${WORKDIR}/variables.ini"

  export \
    TEST_PLANS_VAR_FILE
}

create_rootfs_vars_for_devicetype() {

  BUNDLED_URL=${1}
  ROOTFS_IMAGE_NAME=${2}

  rootfs_image="rootfs"
  case "${ROOTFS_IMAGE_NAME}" in
    "lkft-console-image")
      rootfs_image="rootfs.console"
      ;;
    "lkft-kselftest-image")
      rootfs_image="rootfs.kselftest"
      ;;
    "lkft-tux-image")
      rootfs_image="rootfs.tux"
      ;;
  esac

  if [ -n "${bundle_json}" ] && [ -z "${ROOTFS_URL}" ]; then
    case "${ROOTFS_MACHINE}" in
      am57xx-evm)
        case ${DEVICE_TYPE} in
          x15)                   rootfs_ext="ext4gz" ;;
          juno-r2)               rootfs_ext="tarxz"  ;;
          qemu_arm | qemu_arm64) rootfs_ext="ext4gz" ;;
        esac
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle "${rootfs_image}.${rootfs_ext}" "${bundle_json}")"
        ;;

      dragonboard-410c | dragonboard-845c)
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle ${rootfs_image}.ext4gz "${bundle_json}")"
        ;;

      hikey)
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle ${rootfs_image}.ext4gz "${bundle_json}")"
        ;;

      intel-core2-32)
        case ${DEVICE_TYPE} in
          i386)                    rootfs_ext="tarxz"  ;;
          qemu_i386 | qemu_x86_64) rootfs_ext="ext4gz" ;;
        esac
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle "${rootfs_image}.${rootfs_ext}" "${bundle_json}")"
        ;;

      intel-corei7-64)
        case ${DEVICE_TYPE} in
          x86)         rootfs_ext="tarxz" ;;
          qemu_x86_64) rootfs_ext="ext4gz" ;;
        esac
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle "${rootfs_image}.${rootfs_ext}" "${bundle_json}")"
        ;;

      juno)
        case ${DEVICE_TYPE} in
          bcm2711-rpi-4-b)    rootfs_ext="tarxz" ;;
          juno-r2)            rootfs_ext="tarxz" ;;
          qemu_arm64)         rootfs_ext="ext4gz" ;;
        esac
        # shellcheck disable=SC2034
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle "${rootfs_image}.${rootfs_ext}" "${bundle_json}")"
        ;;

      ls2088ardb)
        ROOTFS_URL="${BUNDLED_URL}/$(get_from_bundle ${rootfs_image}.tarxz "${bundle_json}")"
        ;;
    esac
  fi

  check_and_write ROOTFS_URL
  check_and_write ROOTFS_URL_COMP

}

create_kernel_vars_for_devicetype() {
  echo
  echo "====================================================="
  echo "Now submitting jobs for ${DEVICE_TYPE^^}"
  echo "Using ${build_json} for build info"

  unset DTB_FILENAME
  BUNDLED_URL=${1}
  BOOT_BUNDLE="${BUNDLED_URL}/bundle.json"
  USE_MODULES="${USE_MODULES:-"1"}"
  USE_EXT_KERNEL_TOOLS="${USE_EXT_KERNEL_TOOLS:-"0"}"

  if [ -v GITLAB_CI ]; then
    if [ ! -f "${build_json}" ]; then
      echo "Artifact ${build_json} not found. Can't continue."
      exit 1
    fi
    GIT_DESCRIBE="$(jq -r '.builds | .[] | .git_describe' "${build_json}")"
    DOWNLOAD_URL="$(jq -r '.builds | .[] | .download_url' "${build_json}")"
    MAKE_KERNELVERSION="$(jq -r '.builds | .[] | .kernel_version' "${build_json}")"
    TOOLCHAIN="$(jq -r '.builds | .[] | .toolchain' "${build_json}")"
    # The URL ends with /, so remove the last one
    KERNEL_PUB_DEST="${DOWNLOAD_URL%*/}"
    BUILD_URL="${CI_PIPELINE_URL}"
    BUILD_NUMBER="${CI_BUILD_ID}"
    KERNEL_NAME="$(jq -r '.builds | .[] | .tuxmake_metadata.results.artifacts.kernel[0]' "${build_json}")"
    KERNEL_CONFIG="$(jq -r '.builds | .[] | .tuxmake_metadata.results.artifacts.config[0]' "${build_json}")"
    MODULES_NAME="$(jq -r '.builds | .[] | .tuxmake_metadata.results.artifacts.modules[0]' "${build_json}")"

    gen_build_name_file="/tmp/gen-build-name.py"
    cat << EOF > "${gen_build_name_file}"
import sys
import json
import hashlib

with open(sys.argv[1]) as fp:
    builds = json.load(fp)

build_name = None
kconfig = None
toolchain = None
for ksuid, build in builds['builds'].items():
    build_name = build['build_name']
    kconfig = build['kconfig']
    toolchain = build['toolchain']
    break

if build_name:
    print(build_name)
else:
    name = toolchain
    configs = kconfig
    name += f'-{configs[0]}'
    configs = configs[1:]
    if len(configs):
        sha = hashlib.sha1()
        for config in configs:
            sha.update(f'{config}'.encode())
        name += '-' + sha.hexdigest()[0:8]
    print(name)
EOF
    BUILD_NAME="$(python3 "${gen_build_name_file}" "${build_json}")"

  else
    KERNEL_NAME=Image
    case "${DEVICE_TYPE}" in
      x15 | qemu_arm)
        KERNEL_NAME=zImage
        ;;
      x86 | qemu_x86_64 | i386 | qemu_i386)
        KERNEL_NAME=bzImage
        ;;
    esac
  fi

  KERNEL_URL=${KERNEL_PUB_DEST}/${KERNEL_NAME}

  bundle_json="$(download_and_cache "${BOOT_BUNDLE}")"

  if [ ! -f "${bundle_json}" ]; then
    # Try with Tuxpub
    if ! command -v envsubst > /dev/null; then
      apt update && apt install -y gettext-base
    fi
    curl -sSL https://gitlab.com/Linaro/lkft/rootfs/dir2bundle/-/archive/master/dir2bundle-master.tar.gz | tar zxf - --overwrite
    dir_json="$(download_and_cache "${BUNDLED_URL}/?export=json")"
    bundle_json="$(mktemp)"
    jq -r '.files[].Url' "${dir_json}" | sed -e "s|${BUNDLED_URL}/||g" | ./dir2bundle-master/dir2bundle "${ROOTFS_MACHINE}" > "${bundle_json}"
  fi

  if [ -n "${bundle_json}" ] && [ -z "${BOOT_URL}" ]; then
    case "${ROOTFS_MACHINE}" in
      dragonboard-410c)
        BOOT_URL="${BUNDLED_URL}/$(get_from_bundle boot "${bundle_json}")"
        ;;

      hikey)
        BOOT_URL="${BUNDLED_URL}/$(get_from_bundle boot "${bundle_json}")"
        ;;

    esac
  fi
  case "${DEVICE_TYPE}" in
    bcm2711-rpi-4-b)
      # Raspberry Pi 4B
      DTB_FILENAME=dtbs/broadcom/bcm2711-rpi-4-b.dtb
      ;;
    dragonboard-410c)
      # Qualcomm's Dragonboard 410c
      DTB_FILENAME=dtbs/qcom/apq8016-sbc.dtb
      ;;
    dragonboard-845c)
      # Qualcomm's Dragonboard 845c
      DTB_FILENAME=dtbs/qcom/sdm845-db845c.dtb
      BOOT_URL=${KERNEL_URL}
      ;;
    hi6220-hikey)
      # HiKey
      DTB_FILENAME=dtbs/hisilicon/hi6220-hikey.dtb
      ;;
    juno-r2 | qemu_arm64)
      # Arm's Juno
      DTB_FILENAME=dtbs/arm/juno-r2.dtb
      if [ "${SKIPGEN_KERNEL_VERSION}" = "linux-4.4.y" ]; then
        DTB_FILENAME=dtbs/arm/juno-r1.dtb
      fi
      if [ "${DEVICE_TYPE}" = "qemu_arm64" ]; then
        unset DTB_FILENAME
        BOOT_URL=${KERNEL_URL}
        GS_MACHINE="${GS_MACHINE:-"virt-2.10,accel=kvm"}"
        QEMU_CPU_VARIABLES="${QEMU_CPU_VARIABLES:-"max"}"
      fi
      ;;
    nxp-ls2088)
      # NXP's LS2088A RDB
      DTB_FILENAME=dtbs/freescale/fsl-ls2088a-rdb.dtb
      ;;
    x15 | qemu_arm)
      # am57xx-evm
      DTB_FILENAME=dtbs/am57xx-beagle-x15.dtb
      BOOT_URL=${KERNEL_URL}
      # shellcheck disable=SC2034
      if [ "${DEVICE_TYPE}" = "qemu_arm" ]; then
        unset DTB_FILENAME
      fi
      ;;
    x86 | qemu_x86_64)
      # intel-corei7-64
      BOOT_URL=${KERNEL_URL}
      ;;
    i386 | qemu_i386)
      # intel-core2-32
      # shellcheck disable=SC2034
      BOOT_URL=${KERNEL_URL}
      ;;
  esac

  if [[ $DEVICE_TYPE == *"qemu_"* ]]; then
    # shellcheck disable=SC2034
    OVERLAY_MODULES_URL=${KERNEL_PUB_DEST}/${MODULES_NAME}
  else
    # shellcheck disable=SC2034
    MODULES_URL=${KERNEL_PUB_DEST}/${MODULES_NAME}
  fi
  if [[ "${USE_MODULES}" == "0" ]]; then
    unset OVERLAY_MODULES_URL
    unset MODULES_URL
  fi
  if [[ ${USE_EXT_KERNEL_TOOLS} == "1" ]]; then
    KERNEL_TOOLS_URL=${KERNEL_TOOLS_URL:-${KERNEL_PUB_DEST}}

    if [ "${latest_lts_release}" -eq 1 ]; then
      KERNEL_TOOLS_URL=${KERNEL_PUB_DEST}
    fi

    ktools_metadata_json="$(download_and_cache "${KERNEL_TOOLS_URL}/metadata.json")"
    kselftest="$(jq -r '.results.artifacts.kselftest[0]' "${ktools_metadata_json}")"
    cpupower="$(jq -r '.results.artifacts.cpupower[0]' "${ktools_metadata_json}")"
    perf="$(jq -r '.results.artifacts.perf[0]' "${ktools_metadata_json}")"

    if [[ -n ${kselftest} && ${kselftest} != "null" ]]; then
      # shellcheck disable=SC2034
      OVERLAY_KSELFTEST_URL="${KERNEL_TOOLS_URL}/${kselftest}"
    fi
    if [[ -n ${cpupower} && ${cpupower} != "null" ]]; then
      # shellcheck disable=SC2034
      OVERLAY_CPUPOWER_URL="${KERNEL_TOOLS_URL}/${cpupower}"
    fi
    if [[ -n ${perf} && ${perf} != "null" ]]; then
      # shellcheck disable=SC2034
      OVERLAY_PERF_URL="${KERNEL_TOOLS_URL}/${perf}"
    fi
  fi

  cat << EOF >> "${TEST_PLANS_VAR_FILE}"
DEVICE_TYPE=${DEVICE_TYPE}
KERNEL_ARCH_ARTIFACTS_PUB_DEST=${KERNEL_PUB_DEST}
BUILD_NUMBER=${BUILD_NUMBER}
BUILD_URL=${BUILD_URL}
KERNEL_URL=${KERNEL_URL}
KERNEL_CONFIG_URL=${KERNEL_PUB_DEST}/${KERNEL_CONFIG}
#
KERNEL_DESCRIBE=${GIT_DESCRIBE}
KERNEL_COMMIT=${KERNEL_SHA}
MAKE_KERNELVERSION=${MAKE_KERNELVERSION}
TOOLCHAIN=${TOOLCHAIN}
SKIPGEN_KERNEL_VERSION=${SKIPGEN_KERNEL_VERSION}
KERNEL_BRANCH=${KERNEL_BRANCH}
KERNEL_REPO_URL=${KERNEL_REPO_URL}
BUILD_NAME=${BUILD_NAME}
EOF

  case "${DEVICE_TYPE}" in
    bcm2711-rpi-4-b | juno-r2 | nxp-ls2088)
      echo "KERNEL_URL_COMP=gz" >> "${TEST_PLANS_VAR_FILE}"
      ;;
  esac

  if [ -v DTB_FILENAME ] && [ -n "${DTB_FILENAME}" ]; then
    EXT_DTB_URL=${EXT_DTB_URL:-${KERNEL_PUB_DEST}}
    echo "DTB_URL=${EXT_DTB_URL}/${DTB_FILENAME}" >> "${TEST_PLANS_VAR_FILE}"
  fi

  if [ -v GS_MACHINE ] && [ -n "${GS_MACHINE}" ]; then
    echo "GS_MACHINE=\"${GS_MACHINE}\"" >> "${TEST_PLANS_VAR_FILE}"
  fi
  if [ -v QEMU_CPU_VARIABLES ] && [ -n "${QEMU_CPU_VARIABLES}" ]; then
    echo "QEMU_CPU_VARIABLES=\"${QEMU_CPU_VARIABLES}\"" >> "${TEST_PLANS_VAR_FILE}"
  fi
  for var in \
    OVERLAY_CPUPOWER_URL \
    OVERLAY_KSELFTEST_URL \
    OVERLAY_PERF_URL \
    TARGET_BOOT_TIMEOUT \
    BOOT_URL \
    DTB_FILENAME \
    MODULES_URL \
    OVERLAY_MODULES_URL; do
    check_and_write ${var}
  done

}

project_specific_variables() {
  TDEFINITIONS_REVISION=${TDEFINITIONS_REVISION:-"master"}
  TEST_DEFINITIONS_REPOSITORY=${TEST_DEFINITIONS_REPOSITORY:-"https://github.com/Linaro/test-definitions.git"}

  cat << EOF >> "${TEST_PLANS_VAR_FILE}"
# hikey:
# common, need adjustment per project:
PROJECT_NAME=${QA_PROJECT}
LAVA_JOB_PRIORITY=${LAVA_JOB_PRIORITY}
TDEFINITIONS_REVISION=${TDEFINITIONS_REVISION}
TEST_DEFINITIONS_REPOSITORY=${TEST_DEFINITIONS_REPOSITORY}
PROJECT=projects/lkft/
TAGS=${LAVA_TAGS}
KVM_UNIT_TESTS_REVISION=ca85dda2671e88d34acfbca6de48a9ab32b1810d
EOF

  KSELFTEST_PATH="${KSELFTEST_PATH:-"/opt/kselftests/mainline/"}"

  check_and_write KSELFTEST_PATH
}

# $1: Branch, in the format "linux-4.14.y", "mainline", "next". Required.
# From the environment:
#   LAVA_JOB_PRIORITY: If defined, it will return this value.
get_branch_priority() {
  local branch=""

  # Can be overridden via environment
  if [ -v LAVA_JOB_PRIORITY ]; then
    echo "${LAVA_JOB_PRIORITY}"
    return
  fi

  if [ $# -lt 1 ]; then
    return 1
  fi

  branch=$1

  prio=10

  case ${branch} in
    linux-4.4.y)     prio=71 ;;
    linux-4.9.y)     prio=72 ;;
    linux-4.14.y)    prio=73 ;;
    linux-4.19.y)    prio=74 ;;
    linux-5.4.y)     prio=75 ;;
    linux-5.10.y)    prio=76 ;;
    linux-5.15.y)    prio=77 ;;
    linux-6.0.y)     prio=78 ;;
    linux-6.1.y)     prio=79 ;;
  esac

  echo ${prio}
}

# Get the hash of any string
hash_string() {
  echo -n "$1" | sha1sum | cut -c1-40
}

# $1: URL to download and cache
# From the environment:
#   WORKSPACE
#
# Returns the absolute path and file name of the downloaded resource.
download_and_cache() {
  hashed_url="$(hash_string "${1}")"
  output_file="${WORKDIR}/.downloads/${hashed_url}"

  mkdir -p "${WORKDIR}/.downloads/"

  if [ ! -s "${output_file}" ]; then
    curl -sSLf -o "${output_file}" "${1}" || true
  fi
  if [ -s "${output_file}" ]; then
    readlink -e "${output_file}"
  fi
}

# START HERE

testplan_device_path="--testplan-device-path projects/lkft/devices"
LAVA_TEST_PLANS_URL=${LAVA_TEST_PLANS_URL:-""}
LAVA_TEST_PLANS_ORIGIN=${LAVA_TEST_PLANS_ORIGIN:-"origin"}
LAVA_TEST_PLANS_BRANCH=${LAVA_TEST_PLANS_BRANCH:-"master"}
LAVA_TEST_PLANS_DIR="/usr/local/lib/python3.9/site-packages/lava_test_plans"

if [ -v DRY_RUN ]; then
  DRY_RUN="--dry-run"
else
  DRY_RUN=""
fi
export DRY_RUN

if [ -v CI ]; then
  WORKDIR="${CI_PROJECT_DIR}"
else
  WORKDIR="$(dirname "$(readlink -e "$0")")/workspace"
fi
mkdir -p "${WORKDIR}"

if [ -n "${LAVA_TEST_PLANS_URL}" ]; then
  apt update && apt install -y git
  LAVA_TEST_PLANS_DIR="${WORKDIR}${LAVA_TEST_PLANS_DIR}"
  git clone -b "${LAVA_TEST_PLANS_BRANCH}" "${LAVA_TEST_PLANS_URL}" "${LAVA_TEST_PLANS_DIR}"
  echo "lava-test-plans revision $(git -C "${LAVA_TEST_PLANS_DIR}" rev-parse HEAD)"
fi

if [ -v DEVICE_TYPE ]; then
  # Define DEVICE_TYPE and ROOTFS_MACHINE
  check_and_set ROOTFS_MACHINE ""
  if [ -z "${ROOTFS_MACHINE}" ]; then
    devicetype_to_rootfsmachine "${DEVICE_TYPE}"
  fi

  if [ -v ROOTFS_TUXPLAN_ID ] && [ -n "${ROOTFS_TUXPLAN_ID}" ] &&
     [ ! -v ROOTFS_DEPOT ] || [ -z "${ROOTFS_DEPOT}" ]; then
    pip --quiet --no-cache-dir install tuxsuite
    export TUXSUITE_PROJECT="${TUXSUITE_PROJECT:-lkft}"
    # There must be an API-y way to do this
    tuxplan_machine_id="$(tuxsuite plan get "${ROOTFS_TUXPLAN_ID}" | grep " ${ROOTFS_MACHINE}$" | awk '{print $1}')"
    export ROOTFS_DEPOT="https://storage.tuxsuite.com/public/linaro/${TUXSUITE_PROJECT}/oebuilds/${tuxplan_machine_id}/images"
  fi

  # Variables that can be overrided...
  # =======================================================================
  check_and_set BOOT_URL ""
  check_and_set ROOTFS_URL ""
  check_and_set ROOTFS_DEPOT "https://storage.lkft.org/rootfs/oe-kirkstone/20220824-114729"
  check_and_set ROOTFS_IMAGE_NAME ""
  check_and_set LAVA_TAGS "production"
  if [ ! -v QA_ENVIRONMENT ]; then
    QA_ENVIRONMENT=""
  else
    QA_ENVIRONMENT="--environment ${QA_ENVIRONMENT}"
  fi
  # =======================================================================

  build_json="build.json"
  if [ $# -ge 1 ]; then
    build_json=$1
  fi

  latest_lts_release=0
  if [ "${KERNEL_BRANCH}" == "linux-6.0.y" ]; then
    latest_lts_release=1
  fi

  # Define variables for submit_for_testing
  define_sft_vars_for_machine
  rm -f "${TEST_PLANS_VAR_FILE}"

  BUNDLED_URL="${BUNDLED_URL:-"${ROOTFS_DEPOT}/${ROOTFS_MACHINE}"}"
  # Write kernel related variables
  create_kernel_vars_for_devicetype "${BUNDLED_URL}"
  # If BUILD_ID is undefined, use the kernel's git-describe
  check_and_set BUILD_ID "${GIT_DESCRIBE}"

  # Define LAVA job priority
  lava_job_priority="$(get_branch_priority "${KERNEL_BRANCH}")"
  export LAVA_JOB_PRIORITY=${lava_job_priority}

  # Write rootfs related variables
  create_rootfs_vars_for_devicetype "${BUNDLED_URL}" "${ROOTFS_IMAGE_NAME}"

  # Write project specific variables
  # shellcheck disable=SC2153
  project_specific_variables

  if [ -v EXTRA_TEST_PLANS_VAR_FILE ]; then
    if [ -f "${EXTRA_TEST_PLANS_VAR_FILE}" ]; then
      cat "${EXTRA_TEST_PLANS_VAR_FILE}" >> "${TEST_PLANS_VAR_FILE}"
    fi
  fi
  echo
  echo "---vvv------${TEST_PLANS_VAR_FILE}------vvv---"
  cat "${TEST_PLANS_VAR_FILE}"
  echo "---^^^------${TEST_PLANS_VAR_FILE}------^^^---"

  generate_submit_tests
fi
